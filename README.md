# AmazonBot

A group of robots to perform and simulate actions like the process of shopping products or create accounts on the Amazon website. The idea is getting a better position of a product in the listings.

### Robots
Until now, the project has two robots:

1. **AccountCreatorRobot** - Creates Amazon accounts. 

2. **ShoppingRobot** - Promotes a product in Amazon searching for it by a keyword. Simulates some user actions and adds in cart.

---
## Getting Started

First, clone the repository and go to the project folder:

    $ git clone https://gitlab.com/upfreelas/amazonbot.git
    $ cd amazonbot

### Prerequisites

#### Python
Download and install Python 3.7.1 from [Python.org](https://www.python.org/downloads/release/python-371/). To install the Python packages, run the command:

    $ pip install -r requirements.txt

#### Google Chrome and ChromeDriver
Install Google Chrome on your computer too. After, go to [ChromeDriver](http://chromedriver.chromium.org/downloads) page and download a ChromeDriver according to your installed Google Chrome.

Create a folder called *chromedriver* in the project folder and put the downloaded ChromeDriver executable there.

---
## Usage

On the project folder, to use AccountCreatorRobot, type:

    $ python .\src\run_accountcreatorrobot.py '.\data\Uncreated Accounts - Yahoo Unique DATA.csv'

The *Uncreated Accounts - Yahoo Unique DATA.csv* is a simple CSV file with customers information. The robot will go through this file and it will be creating the Amazon accounts. The same file will be updated when account creation is successful with "OK". If Amazon presents a captcha or raises an internal error, the file won't be updated.

Keep in mind that, *Uncreated Accounts - Yahoo Unique DATA.csv* file is used by ShoppingRobot to simulates login on Amazon.

To use ShoppingRobot, type:

    $ python .\src\run_shoppingrobot.py

As this robot is more complex than AccountCreatorRobot, so its configurations are passed by an .INI file, like [shoppingrobot.ini](https://gitlab.com/upfreelas/amazonbot/blob/master/shoppingrobot.ini). So, know the important parameters:

* [DRIVER] page_load_timeout - Time in seconds until the driver waits for a response from Amazon. Be careful, low values can raise many errors.
* [DRIVER] load_images - If True, the robot will load images during execution. Disable, if you are using a proxy service.
* [PROXY] active - If True, the robot will use proxy service.


### The ShoppingRobot will...
1. choose (randomly) a product and a related keyword.
2. type the keyword on Amazon search bar
3. go through the listings until finding the product using ASIN code.
4. simulate client actions like see images, reviews, etc.
5. add the product on the cart
6. wait for a little and close the browser.
