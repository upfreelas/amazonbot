import sys
from common import support
from datetime import datetime as dt
from robots.accountcreatorrobot import AccountCreatorRobot


def run_a_thread(file):
    customers = support.load_customers(file)

    for customer in customers:

        # If customer has a Amazon account, go to the next.
        if customer['Amazon account created']:
            continue

        start_time = dt.now()

        robot = AccountCreatorRobot()
        robot.logger.info('Creating %s (%s / %s) Amazon account.' % (customer['name'],
                                                                     customer['Email'],
                                                                     customer['Password']))
        try:
            while True:
                robot.create_browser(user_agent=customer['user_agent'])
                if not robot.go_to_initial_page():
                    robot.quit()
                    continue
                break

            robot.go_to_account_creation_page()
            robot.fill_form(name=customer['name'],
                            email=customer['Email'],
                            password=customer['Password'])

            if robot.any_error_message():
                robot.quit()
                continue

            if robot.any_authportal_message():
                robot.quit()
                continue

            if robot.verification_needed():
                code = robot.wait_for_verification_code(email=customer['Email'],
                                                        password=customer['Password'])
                robot.enter_the_code(code)

            if robot.is_logged_in():
                customer['Amazon account created'] = 'OK'

               
            robot.logger.info('Saving on %s' % file)
            support.save_customers(file, customers)
            end_time = dt.now()
            robot.logger.info(end_time - start_time)
            robot.quit()
        except Exception:
            robot.logger.info('!!! An unexpected error occured. !!!')
            robot.quit()

def main():
    file = sys.argv[1]
    run_a_thread(file)


if __name__ == "__main__":
    main()
