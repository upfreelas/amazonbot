import os
import pytz
import socket
import inflect
import tzlocal
import logging
import lxml.html
from time import sleep
from csv import DictWriter
from selenium import webdriver
from random import randint, choice
from customers.mailbox import MailBox
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from datetime import datetime as dt, timedelta as td
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException, NoSuchElementException

def get_eltm_status(elmt):
    """Search for "Sponsored" text inside an element.

    Parameters
    ----------
    elmt : str
        The status of element (sponsored or not).
    """
    try:
        elmt.find_element_by_xpath('.//*[text()="Sponsored"]')
        return 'sponsored'
    except NoSuchElementException:
        return 'non_sponsored'

def search_elmt_by_asin(listing, asins):
    """Searches for ASIN code inside an listing element.


    Parameters
    ----------
    listing : selenium.webdriver.remote.webelement.WebElement
        The WebElement to search inside.
    asins : list
        The ASIN codes that will be search.
    
    Return
    ------
    found_elmt : list
        The found elements.
    """
    found_elmt = []
    elmts = listing.find_elements_by_xpath('.//*[@data-asin]')
    for elmt in elmts:
        asin = elmt.get_attribute('data-asin')
        if asin in asins:
            found_elmt.append(elmt)
    
    return found_elmt

def random_sleep(min=3, max=7):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.
    
    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
        
    """
    if max <= min:
        max = min + 3
    sleep(randint(min, max))

def get_logger(name):
    """Creates a instance logger

    Return
    ------
    logger : logging.Logger
        A logger itself.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # Set a handler
    if not logger.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        # Set a formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(process)s - %(message)s')
        handler.setFormatter(formatter)
        # Add handler to logger
        logger.addHandler(handler)
    return logger

class ShoppingRobot():
    def __init__(self,
                 initial_page='https://amazon.com',
                 wait_time=10,
                 use_login=False,
                 customer_email=None,
                 customer_pwd=None,
                 local_timezone=None):
        self.logger = get_logger('ShoppingRobot')
        self.logger.info('Initializing a robot.')
        self.INITITAL_PAGE = initial_page
        self.WAIT_TIME = wait_time
        
        if use_login:
            self.logger.info('Login ENABLED: %s / %s' % (customer_email,
                                                         customer_pwd))
            self.CUSTOMER_EMAIL = customer_email
            self.CUSTOMER_PWD = customer_pwd
            self.USE_LOGIN = use_login
            # Local timezone
            if local_timezone:
                self.TIMEZONE = pytz.timezone(local_timezone)
            else:
                self.TIMEZONE = tzlocal.get_localzone()
        
        # The header of outputfile.
        self.HEADER = ['MACHINE',
                       'DATETIME',
                       'PROXY_HOST',
                       'PROXY_USER',
                       'CLIENT_EMAIL',
                       'TARGETS_ASIN',
                       'KEYWORD',
                       'FIRST_FOUND',
                       'FOUND_ON_PAGE',
                       'CHOSEN_ASIN',
                       'QUANTITY',
                       'TIME_ON_PAGE']
        # Creates a dict that will be used to save on output file.
        self.OUTPUT = {k: None for k in self.HEADER}
        self.PLURAL = inflect.engine()

    def submit_proxy_credentials(self, proxy_user, proxy_pwd):
        """If use_proxy is True, fill the extension proxy form to authenticate on server.
        Avoids proxy authentication popup.

        Parameters
        ----------
        proxy_user: str
            The proxy username.
        proxy_pwd: str
            The proxy password.
        """
        self.WAIT.until(lambda d: d.find_element_by_id('login')).send_keys(proxy_user)
        self.WAIT.until(lambda d: d.find_element_by_id('password')).send_keys(proxy_pwd)
        self.WAIT.until(lambda d: d.find_element_by_id('save')).click()

    def create_browser(self,
                       chromedriver_path='chromedriver/chromedriver.exe',
                       use_proxy=True,
                       proxy_extension_path='chromedriver/ProxyAutoAuth2-0.crx',
                       proxy_host=None,
                       proxy_user='ZachKurian',
                       proxy_pwd='8e4bJZuP',
                       user_agent=None,
                       page_load_timeout=20,
                       load_images=False):
        """Creates a browser.

        Parameters
        ----------
        chromedriver_path : str
            The path to the driver executable.
        use_proxy : bool
            If True, the browser will use proxy.
        proxy_extension_path : str
            The path to proxy extension.
        proxy_host : str
            The proxy host (server:port). If not informed, the bot creates one.
        proxy_user: str
            The proxy username.
        proxy_pwd: str
            The proxy password.
        user_agent : str
            The USER_AGENT string. If use_login is True during ShoppingRobot
            class initialization,  the customer_ua parameter overrides this
            parameter.
        page_load_timeout : int
            The page load time out in seconds.
        load_images : bool
            If True, the browser will download images. Be carefull, it can
            overload the proxy
        """
        self.logger.info('A browser is being created.')
        opt = webdriver.ChromeOptions()
        opt.add_argument('--lang=en-us')
        opt.add_argument("--log-level=3")
        opt.add_argument("--silent")

        if not load_images:
            self.logger.info('Download images DISABLED.')
            prefs = {'profile.managed_default_content_settings.images':2}
            opt.add_experimental_option("prefs", prefs)
        else:
            self.logger.info('Download images ENABLED.')

        if use_proxy:
            if not proxy_host:
                proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
            opt.add_argument('--proxy-server=%s' % proxy_host)
            opt.add_extension(proxy_extension_path)
            self.logger.info('Proxy ENABLED: %s - %s / %s' % (proxy_host,
                                                              proxy_user,
                                                              proxy_pwd))
        else:
            self.logger.info('Proxy DISABLED.')

        self.logger.info('Page load time: %s seconds.' % page_load_timeout)

        if user_agent:
            opt.add_argument('user-agent=%s' % user_agent)

        self.DRIVER = webdriver.Chrome(executable_path=chromedriver_path,
                                       options=opt)
        self.DRIVER.set_page_load_timeout(page_load_timeout)
        self.ACTION = ActionChains(self.DRIVER)
        self.WAIT = WebDriverWait(self.DRIVER, self.WAIT_TIME)

        if use_proxy:
            self.submit_proxy_credentials(proxy_user, proxy_pwd)
            self.OUTPUT['PROXY_HOST'] = proxy_host
            self.OUTPUT['PROXY_USER'] = proxy_user

    def go_to_initial_page(self):
        """Goes to initial page sets during initialization.
        """
        try:
            self.DRIVER.get(self.INITITAL_PAGE)
        except Exception:
            pass
        
        # No internet page?
        try:
            error = self.DRIVER.find_element_by_id('error-information-popup-content')
            error_msg = error.find_element_by_xpath('//*[@class="error-code"]').text
            self.logger.warning('No internet: %s' % error_msg)
            return False
        except NoSuchElementException:
            pass
        
        # Did not find the link HTML tag. So it did not work.
        try:            
            url = self.WAIT.until(
                lambda d: d.find_element_by_xpath('//link[@rel="canonical"]')
            ).get_attribute('href')
            self.logger.info('Accessing: %s' % url)
        except TimeoutException:
            self.logger.info('TimeoutException: Initial page did not load.')
            return False
        
        return True

    def go_to_atc_page(self,asin):
        """Goes to add to cart page.
        """
        try:
            qty = randint(3,9)
            atcurl = "https://www.amazon.com/gp/aws/cart/add.html?ASIN.1=" + asin + "&Quantity.1=" + str(qty)
            self.logger.info('Add to cart url: %s' % atcurl)
            self.DRIVER.get(atcurl)
            random_sleep(3,7)
            self.WAIT.until(lambda d: d.find_element_by_name('add')).click()
        except Exception:
            pass

        # No internet page?
        try:
            error = self.DRIVER.find_element_by_id('error-information-popup-content')
            error_msg = error.find_element_by_xpath('//*[@class="error-code"]').text
            self.logger.warning('No internet: %s' % error_msg)
            return False
        except NoSuchElementException:
            pass

        # Did not find the link HTML tag. So it did not work.
        try:
            url = self.WAIT.until(
                lambda d: d.find_element_by_xpath('//link[@rel="canonical"]')
            ).get_attribute('href')
            self.logger.info('Accessing: %s' % url)
        except TimeoutException:
            self.logger.info('TimeoutException: Initial page did not load.')
            return False

        return True
    
    def quit(self):
        """Close the entire browser
        """
        self.logger.info('Closing the browser with id (%s).' % id(self.DRIVER) )
        self.DRIVER.quit()

    def sign_in(self):
        """Only goes to the login page.
        """
        self.logger.info('Going to login page.')
        try:
            self.WAIT.until(lambda d: d.find_element_by_id('nav-signin-tooltip')).click()
            random_sleep(min=1, max=2) # zZzZz...
            return True
        except TimeoutException:
            pass
        # If did not find "Sign in" button, click on "Accounts & Lists".
        nav_accountlist = self.WAIT.until(lambda d: d.find_element_by_id('nav-link-accountList'))
        
        try:
            self.ACTION.move_to_element(nav_accountlist).perform()
            self.WAIT.until(lambda d: d.find_element_by_id('nav-flyout-ya-signin')).click()
            random_sleep(min=1, max=2) # zZzZz...
            return True
        except TimeoutException:
            pass
        
        WebDriverWait(self.DRIVER, self.WAIT_TIME).until(
            ec.visibility_of_element_located((By.LINK_TEXT, 'Your Lists'))
        ).click()
        random_sleep(min=1, max=2) # zZzZz...
    
    def fill_login_form(self):
        """Fills the login form with customer information.
        """
        self.logger.info('Filling the login form.')
        self.WAIT.until(lambda d: d.find_element_by_id('ap_email')).send_keys(self.CUSTOMER_EMAIL)
        self.WAIT.until(lambda d: d.find_element_by_id('continue')).click()
        random_sleep(min=1, max=2) # zZzZz...
        self.WAIT.until(lambda d: d.find_element_by_id('ap_password')).send_keys(self.CUSTOMER_PWD)
        random_sleep(min=1, max=2) # zZzZz...
        self.WAIT.until(lambda d: d.find_element_by_id('signInSubmit')).click()
        random_sleep(min=1, max=2) # zZzZz...
        self.OUTPUT['CLIENT_EMAIL'] = self.CUSTOMER_EMAIL

    def verification_needed(self):
        """Verifies if Amazon shows a verification page. And clicks on "Continue"
        or "Send Code" button.

        Return
        ------
        success : bool
            If True, Amazon took the robot to verification page. Otherwise,
            go shopping.
        """
        # Verification needed?
        try:
            verify_form_xpath = '//form[@name="claimspicker" and @action="verify"]'
            self.WAIT.until(lambda d: d.find_element_by_xpath(verify_form_xpath))
            self.logger.warning('Verification needed.')
            random_sleep(min=1, max=2) # zZzZz...
            self.WAIT.until(lambda d: d.find_element_by_id('continue')).click()
            return True
        except TimeoutException:
            return False

    def wait_for_verification_code(self, attempts=10, time_bw_attempts=5):
        """Waits for the verification code arrives in customer mailbox.

        Parameters
        ----------
        attempts : int
            The number of times that the robot will attempt.
        time_bw_attempts : int
            The time between each attempt.
        
        Return
        ------
        code : str
            The verification code itself
        """
        # Open an IMAP connection to wait for verification email
        self.logger.info('Waiting for verification code in %s attempts.' % attempts)
        mail = MailBox(username=self.CUSTOMER_EMAIL,
                       password=self.CUSTOMER_PWD,
                       server='imap.mail.yahoo.com')
        mail.login()
    
        while (range(attempts)):
            # Current datetime
            now_dt = pytz.utc.localize(dt.utcnow())

            sleep(time_bw_attempts) # zZzZz...

            # Get emails from mailbox. 
            verification_emails = mail.get_emails_by_sender('account-update@amazon.com')
            verification_emails.reverse()

            for e in verification_emails:
                # Before compare datetime, convert to local timezone
                now_dt = now_dt.astimezone(self.TIMEZONE)            
                email_dt = e['date'].astimezone(self.TIMEZONE)

                # Email datetime is less than 10 minutes
                if not now_dt - email_dt > td(minutes=10):
                    date_format = '%H:%M:%S on %Y/%m/%d'
                    self.logger.info('An email with valid code was received in %s.' % email_dt.strftime(date_format))
                    
                    # Convert content to HTML and parse using xpath to search the code.
                    content_in_html = lxml.html.fromstring(e['content'])
                    amz_code = content_in_html.xpath('//p[@class="otp"]/text()')[0]
                    mail.logout()
                    return amz_code

        self.logger.warning('A verification email was not found.' % self.CUSTOMER_EMAIL)                    
        mail.logout()
        
    def enter_the_code(self, code):
        """Enter the verification code.
        
        Parameters
        ----------
        code : str
            The code sent to customer email.
        """
        self.logger.info('Entering the code %s on verification form.' % code)
        self.DRIVER.find_element_by_xpath('//input[@name="code"]').send_keys(code)
        random_sleep(min=1, max=2) # zZzZz...
        self.DRIVER.find_element_by_xpath('//input[@type="submit"]').click()
    
    def click_on_next(self):
        """Clicks on next button
        """
        try:
            self.WAIT.until(lambda d: d.find_element_by_xpath('//li[@class="a-last"]/a')).click()
            return True
        except TimeoutException:
            return False

    def type_on_search_bar(self, keyword):
        """Types a keyword on search bar.
        
        Parameters
        ----------
        keyword : str
            Keyword to be typed in searchbar
        """
        self.OUTPUT['KEYWORD'] = keyword
        self.logger.info('KEYWORD: %s' % keyword)
        # Locates the search bar.
        search_bar = self.WAIT.until(lambda d: d.find_element_by_id('twotabsearchtextbox'))
        search_bar.clear()
        search_bar.send_keys(keyword)
        random_sleep(min=2, max=4) # zZzZz...
        # Clicks in "Go"
        self.WAIT.until(lambda d: d.find_element_by_xpath(
            '//*[@id="nav-search-submit-text"]/following-sibling::input'
        )).click()
        random_sleep(min=2, max=4) # zZzZz...

    def products_on_listing(self, asins, keyword, page_limit=21, stop_at_the_first=True):
        """Retrieves products on listing by ASIN codes.

        Parameters
        ----------
        asins : list
            The list with ASIN codes.
        page_limit : int
            The maximum number of pages that robot can access.
        stop_at_the_first : bool
            If True, the robot returns only the first found product.
        
        Returns
        -------
        products : list
            The list of found products (dicts).
        """
        self.logger.info('ASIN: %s' % asins)
        self.OUTPUT['TARGETS_ASIN'] = asins
        products = []
        
        for npage in range(1, page_limit + 1):
            # Gets the listing element on the page.
            listing = self.WAIT.until(lambda d: d.find_element_by_xpath('//*[@data-component-type="s-search-results"]'))
            # Searches for elements that contain informed ASIN codes.
            elmts_on_page = search_elmt_by_asin(listing=listing, asins=asins)
            # Analyzes each element.
            for i, elmt in enumerate(elmts_on_page):
                product = {'date': dt.now().strftime('%Y-%m-%d %H:%M:%S'),
                           'asin': elmt.get_attribute('data-asin'),
                           'keyword': keyword,
                           'link': elmt.find_element_by_tag_name('a').get_attribute('href'),
                           'page': npage,
                           'status_on_listing': get_eltm_status(elmt)}
                if i == 0:
                    # Output variables
                    self.START_TIME = dt.now()
                    self.OUTPUT['FOUND_ON_PAGE'] = npage
                    self.OUTPUT['FIRST_FOUND'] = product['asin']

                products.append(product)
                self.logger.info('Product %s (%s) found on page %s.' % (product['asin'],
                                                                        product['status_on_listing'],
                                                                        npage))

                if stop_at_the_first:
                    return products

            if page_limit == npage:
                self.logger.info('Page limit (%s) reached.' % page_limit)
                break
            
            # Go to next page.
            if not self.click_on_next():
                random_sleep(min=2, max=4) # zZzZz...
                self.logger.info('Last page of the listing (%s) reached.' % npage)
                break

        if not products:
            self.logger.warning('No product found on listing.')

        return products

    def search_product_by_keyword(self, asins, page_limit=21):
        """Sweeps the listing. If product is found on page. Go to product page.
        
        Parameters
        ----------
        asin : str
            ASIN product code

        Returns
        -------
        product_link : str
            If product is found, return the URL to product page. Otherwise, empty string.
        """
        self.OUTPUT['TARGETS_ASIN'] = asins
        product_link = None

        for npage in range(1, page_limit + 1):
            # Waits for product listing
            listing = self.WAIT.until(lambda d: d.find_element_by_xpath(
                '//*[@data-component-type="s-search-results"]'
            ))
            # Looking for a element with a link that contains ASIN code. Sponsored products have
            # another link pattern
            for asin in asins:
                try:
                    xpath_asin = '//*[contains(@href, "dp/%s/ref")]' % asin
                    link_asin = listing.find_element_by_xpath(xpath_asin)
                    product_link = link_asin.get_attribute('href')
                    self.logger.info('Product %s was found in page %s.' % (asin, npage))
                    self.START_TIME = dt.now()
                    self.OUTPUT['FOUND_ON_PAGE'] = npage
                    self.OUTPUT['FIRST_FOUND'] = asin
                    self.OUTPUT['CHOSEN_ASIN'] = asin
                    self.OUTPUT['QUANTITY'] = 1
                    return product_link
                except Exception:
                    pass
            
            # Next page
            try:
                self.WAIT.until(lambda d: d.find_element_by_xpath('//li[@class="a-last"]/a')).click()
                random_sleep(min=2, max=4) # zZzZz...
                continue
            except Exception:
                page_limit = npage
                break

        # Log unsuccessful search
        self.logger.info('Nothing found until page %s.' % page_limit)
        return product_link

    def get(self, url):
        """Goes to URL.
        url : str
            The defined URL.
        """
        self.DRIVER.get(url)
        random_sleep(min=2, max=4) # zZzZz...

    def see_images(self):
        """Clicks on every thumbnail image of the product. Browser instance must in
        a product page to works.
        """
        try:
            tumbimages = self.WAIT.until(
                lambda d: d.find_element_by_id('altImages')
            ).find_elements_by_xpath('//li[contains(@class, "imageThumbnail")]')
            # Log how many tumbnail images product has
            self.logger.info('Analyzing %s product images' % len(tumbimages))
            # Up to down or reversed
            if choice((True, False)):
                tumbimages.reverse()
            # Browser for all of them
            for img in tumbimages:
                random_sleep(min=1, max=2) # zZzZzZz...
                img.find_element_by_tag_name('input').click()
        except Exception:
            self.logger.info('No thumbnail images was found!')

    def see_and_choose_a_color(self):
        """Clicks on every color variation of the product. Browser instance must in a
        product page to works.
        """
        try:
            # Count how many color variations product has
            color_variations = self.WAIT.until(
                lambda d: d.find_element_by_id('variation_color_name')
            ).find_elements_by_xpath('//li[starts-with(@id, "color_name")]')
            self.logger.info('Analyzing %s color variations' % len(color_variations))
            # Left to right or reversed
            if choice((True, False)):
                color_variations.reverse()
            # Browser for all of them
            for color in color_variations:
                random_sleep() # zZzZzZz...
                color.click()
            random_sleep() # zZzZzZz...
            # Choose a color
            chosen_color = choice(color_variations)
            chosen_asin = chosen_color.get_attribute('data-defaultasin')
            self.logger.info('Product ASIN chosen: %s' % chosen_asin)
            chosen_color.click()
            random_sleep() # zZzZzZz...
            self.OUTPUT['CHOSEN_ASIN'] = chosen_asin
        except Exception:
            self.logger.info('No color variations found!')

    def see_popup_reviews(self):
        """Passes the mouse over the reviews popup. Browser instance must in a
        product page to works.
        """
        self.logger.info('See reviews popup')
        try:
            reviews_popup = self.WAIT.until(lambda d: d.find_element_by_id('acrPopover'))
            self.ACTION.move_to_element(reviews_popup).perform()
            random_sleep() # zZzZzZz...
        except Exception:
            self.logger.info('Product does not have reviews yet.')
            pass

    def see_reviews(self):
        """Go to reviews section. The currentHeight and finalHeight returns
        can be used to scroll down and scroll up.
        
        Returns
        -------
        currentHeight : int
            Where reviews section begins
        finalHeight : int
            Where reviews section ends
        """
        self.logger.info('See reviews section')
        try:
            # Go to reviews
            self.WAIT.until(lambda d: d.find_element_by_id('acrCustomerReviewLink')).click()
            # Get reviews and calculate the beginning and end of element
            reviews = self.WAIT.until(lambda d: d.find_element_by_id('reviewsMedley'))
            currentHeight = self.DRIVER.execute_script("return window.pageYOffset")
            finalHeight = currentHeight + reviews.size['height']
            random_sleep() # zZzZzZz...
            return currentHeight, finalHeight
        except Exception:
            self.logger.info('Product does not have a review section yet.')

            return None, None

    def scroll_down(self, currentHeight = 0, finalHeight = 0):
        """Makes the browser scrolls down.

        Parameters
        ----------
        currentHeight : int
            Scroll movement starts from this value. If not set, will starts from position 0.
        finalHeight : int
            Scroll movement finish in this value. If not set, will finish on the botton of page.
        """
        self.logger.info('Scrolling down')
        if not currentHeight:
            currentHeight = self.DRIVER.execute_script("return window.pageYOffset")
        if not finalHeight:
            finalHeight = self.DRIVER.execute_script("return document.body.scrollHeight")
        if currentHeight < finalHeight: # final is not bigger then current height
            step = int((finalHeight - currentHeight)/20)
            for height in range(currentHeight, finalHeight, step)[0:-1]:
                random_sleep(min=2, max=4) # zZzZzZz...
                for sub_height in range(height, height+step, 20):
                    self.DRIVER.execute_script('window.scrollTo(0, %s);' % sub_height)

    def scroll_up(self, currentHeight = 0, finalHeight = 0):
        """Makes the browser scrolls up.

        Parameters
        ----------
        currentHeight : int
            Scroll movement starts from this value. If not set, will starts from the botton of page.
        finalHeight : int
            Scroll movement finish in this value. If not set, will finish on the top of page.
        """
        self.logger.info('Scrolling up')
        if not currentHeight:
            currentHeight = self.DRIVER.execute_script("return window.pageYOffset")
        if not finalHeight:
            finalHeight = self.DRIVER.execute_script("return document.body.scrollTop")
        if currentHeight > finalHeight: # final is not smaller then current height
            step = int((currentHeight - finalHeight)/20)
            for height in reversed(range(finalHeight, currentHeight, step)[0:-1]):
                random_sleep(min=2, max=4) # zZzZzZz...
                for sub_height in reversed(range(height, height+step, 20)):
                    self.DRIVER.execute_script('window.scrollTo(0, %s);' % sub_height)

    def choose_quantity(self):
        """Choose a quantity of product before insert in the cart. Browser
        instance must in a product page to works.
        """
        quantity = str(randint(1, 5))
        self.logger.info('Choosing %s items' % quantity)
        try:
            quantity_elmt = self.WAIT.until(lambda d: d.find_element_by_id('quantity'))
            select_qty = Select(quantity_elmt)
            select_qty.select_by_value(quantity)
            self.OUTPUT['QUANTITY'] = quantity
        except Exception:
            self.logger.info('Is not possible select quantity.')
        random_sleep() # zZzZzZz...
    
    def add_to_cart(self):
        """Choose a quantity of product before insert in the cart. Browser
        instance must in a product page to works.

        Return
        ------
        success : bool
            If True, the button "Add to cart" is accessible.
        """
        self.logger.info('Adding to cart')
        try:
            self.WAIT.until(lambda d: d.find_element_by_id('add-to-cart-button')).click()
            random_sleep() # zZzZzZz...
            self.OUTPUT['TIME_ON_PAGE'] = dt.now() - self.START_TIME
            return True
        except Exception:
            self.logger.info('"Add to cart" button did not appear.')
        
        return False

    def add_to_your_order_modal(self):
        """If a model offering Furniture Plan appears in a modal pane,
        select one option.
        """
        try:
            furniture_plan = WebDriverWait(self.DRIVER, 2).until(
                ec.visibility_of_element_located((
                    By.XPATH,
                    '//div[starts-with(@id, "a-popover-") and @data-action="a-popover-a11y"]'
            )))
            plan_options = furniture_plan.find_elements_by_xpath('.//button[@id]')
            button = choice(plan_options)
            self.logger.info('Clicking on %s'% button.text)
            button.click()
            random_sleep() # zZzZzZz...
        except Exception:
            self.logger.info('Furniture plan did not appears in a modal pane.')

    def add_to_your_order_side_pane(self):
        """If a model offering Furniture Plan appears in a side pane,
        select one option.
        """
        try:
            attach_warranty = WebDriverWait(self.DRIVER, 2).until(
                ec.visibility_of_element_located((By.ID, 'attach-warranty'))
            )
            header = attach_warranty.find_element_by_xpath('h1').text
            self.logger.info(header)
            plan_options = attach_warranty.find_elements_by_xpath('.//button[@id]')
            button = choice(plan_options)
            self.logger.info('Clicking on %s'% button.text)
            button.click()
            random_sleep() # zZzZzZz...
            
            # Proceed to checkout
            self.logger.info('Proceeding to checkout.')
            self.DRIVER.find_element_by_xpath(
                '//*[@id="attach-sidesheet-checkout-button-announce"]/parent::span'
            ).click()
        except Exception:
            self.logger.info('Furniture plan did not appears in side pane.')

    def click_on_proceed_to_checkout(self):
        """Find and click on "Proceed to checkout" button."
        """
        try:
            self.WAIT.until(lambda d: d.find_element_by_id('hlb-ptc-btn')).click()
        except Exception:
            self.logger.info('"Proceed to checkout" button did not find.')
    
    def find_shipping_address_form(self):
        """Only find if the shipping address form was reached.
        """
        try:
            self.WAIT.until(lambda d: d.find_element_by_xpath(
                '//form[starts-with(@action, "/gp/buy/shipaddressselect")]'
            ))
            self.logger.info('Shipping address form was reached.') 
        except Exception:
            pass

    def empty_cart(self):
        """Emptys the cart.
        """
        try:
            cart_count = self.WAIT.until(lambda d: d.find_element_by_id('nav-cart-count')).text
            if cart_count != '0':
                self.logger.info('%s %s found on cart.' % (cart_count,
                                                           self.PLURAL.plural('item',cart_count)))
                self.WAIT.until(lambda d: d.find_element_by_id('nav-cart')).click()

                self.WAIT.until(lambda d: d.find_element_by_id('sc-active-cart'))

                while cart_count != '0':
                    products = self.WAIT.until(lambda d: d.find_elements_by_xpath('//*[@data-asin and not(@data-removed)]'))
                    product = products.pop()
                    asin = product.get_attribute('data-asin')
                    product.find_element_by_xpath('.//input[@value="Delete"]').click()
                    WebDriverWait(self.DRIVER, self.WAIT_TIME).until(
                        ec.presence_of_element_located((By.XPATH,
                                                        '//*[@data-asin="%s" and @data-removed]' % asin))
                    )
                    
                    cart_count = self.WAIT.until(lambda d: d.find_element_by_id('nav-cart-count')).text
                    self.logger.info('%s removed from cart.' % asin)

            else:
                self.logger.info('No item found on cart.')
        except TimeoutException:
            pass

    def add_to_wish_list(self):
        """Adds the product on Whish list
        """
        try:
            self.WAIT.until(lambda d: d.find_element_by_id('wishListDropDown'))
            self.WAIT.until(lambda d: d.find_element_by_id('add-to-wishlist-button-submit')).click()
            random_sleep() # zZzZzZz...
            self.WAIT.until(lambda d: d.find_element_by_id('WLHUC_continue')).click()
            self.logger.info('There is a Wish List.')
        except TimeoutException:
            self.WAIT.until(lambda d: d.find_element_by_id('add-to-wishlist-button-submit')).click()
            random_sleep() # zZzZzZz...
            self.WAIT.until(lambda d: d.find_element_by_xpath('//*[@id="WLNEW_list_type_WL"]/following::i')).click()
            random_sleep() # zZzZzZz...
            self.WAIT.until(lambda d: d.find_element_by_id('WLNEW_create')).click()
            random_sleep() # zZzZzZz...
            self.WAIT.until(lambda d: d.find_element_by_xpath('//button[text()="Continue shopping"]')).click()
            self.logger.info('Creating a Wish List.')

        self.logger.info('Adding product to Wish List.')
        random_sleep() # zZzZzZz...

    def get_output(self):
        """Gets all OUTPUT variables.

        Return
        ------
        output : list
            THe list with all variables.
        """
        self.logger.info('Gathering all output variables.')
        self.OUTPUT['DATETIME'] = dt.now().strftime('%Y-%m-%d %H:%M:%S')
        self.OUTPUT['MACHINE'] = socket.gethostname()
        output = [str(o) for o in self.OUTPUT.values()]
        return output