import pytz
import inflect
import tzlocal
import lxml.html
from time import sleep
from random import randint
from selenium import webdriver
from customers.mailbox import MailBox
from selenium.webdriver.common.by import By
from common.support import get_logger
from datetime import datetime as dt, timedelta as td
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException, NoSuchElementException


def get_eltm_status(elmt):
    """Search for "Sponsored" text inside an element.

    Parameters
    ----------
    elmt : str
        The status of element (sponsored or not).
    """
    try:
        elmt.find_element_by_xpath('.//*[text()="Sponsored"]')
        return 'sponsored'
    except NoSuchElementException:
        return 'non sponsored'

def search_elmt_by_asin(listing, asins):
    """Searches for ASIN code inside an listing element.


    Parameters
    ----------
    listing : selenium.webdriver.remote.webelement.WebElement
        The WebElement to search inside.
    asins : list
        The ASIN codes that will be search.
    
    Return
    ------
    found_elmt : list
        The found elements.
    """
    found_elmt = []
    elmts = listing.find_elements_by_xpath('.//*[@data-asin]')
    for elmt in elmts:
        asin = elmt.get_attribute('data-asin')
        if asin in asins:
            found_elmt.append(elmt)
    
    return found_elmt

class SimpleRobot():
    def __init__(self, wait_time=10):
        self.logger = get_logger('SimpleRobot')
        self.WAIT_TIME = wait_time

    def submit_proxy_credentials(self, proxy_user, proxy_pwd):
        """If use_proxy is True, fill the extension proxy form to authenticate on server.
        Avoids proxy authentication popup.

        Parameters
        ----------
        proxy_user: str
            The proxy username.
        proxy_pwd: str
            The proxy password.
        """
        self.WAIT.until(lambda d: d.find_element_by_id('login')).send_keys(proxy_user)
        self.WAIT.until(lambda d: d.find_element_by_id('password')).send_keys(proxy_pwd)
        self.WAIT.until(lambda d: d.find_element_by_id('save')).click()

    def create_browser(self, chromedriver_path, proxy_extension_path, proxy_user, proxy_pwd, page_load_timeout=20, load_images=False):
        """Creates a browser.

        Parameters
        ----------
        chromedriver_path : str
            The path to the driver executable.
        proxy_extension_path : str
            The path to proxy extension.
        proxy_host : str
            The proxy host (server:port). If not informed, the bot creates one.
        proxy_user: str
            The proxy username.
        page_load_timeout : int
            The page load time out in seconds.
        load_images : bool
            If True, the browser will download images. Be carefull, it can
            overload the proxy
        """
        opt = webdriver.ChromeOptions()
        opt.add_argument('--lang=en-us')
        opt.add_argument("--log-level=3")
        opt.add_argument("--silent")
        opt.add_experimental_option("detach", True)
        msg = '. '.join(['Detach: True'])
        
        if not load_images:
            msg = '. '.join([msg, 'Images: DISABLED'])
            prefs = {'profile.managed_default_content_settings.images':2}
            opt.add_experimental_option("prefs", prefs)
        else:
            msg = '. '.join([msg, 'Images: ENABLED'])
        
        use_proxy = proxy_extension_path and proxy_user and proxy_pwd
        if use_proxy:
            proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
            opt.add_argument('--proxy-server=%s' % proxy_host)
            opt.add_extension(proxy_extension_path)
        else:
            msg = '. '.join([msg, 'Proxy: DISABLED'])

        self.DRIVER = webdriver.Chrome(executable_path=chromedriver_path,
                                       options=opt)
        self.logger.info('Opening the browser')
        self.DRIVER.set_page_load_timeout(page_load_timeout)
        msg = '. '.join([msg, 'Load Timeout: %s' % page_load_timeout])
        self.ACTION = ActionChains(self.DRIVER)
        self.WAIT = WebDriverWait(self.DRIVER, self.WAIT_TIME)
        self.logger.info(msg)

        if use_proxy:
            self.logger.info('Proxy ENABLED: %s - %s / %s' % (proxy_host,
                                                              proxy_user,
                                                              proxy_pwd))
            self.submit_proxy_credentials(proxy_user, proxy_pwd)

    def get(self, url):
        """Goes to URL.
        
        Parameters
        -----------
        url : str
            The defined URL.
        """
        self.logger.info('Accessing: %s' % url)
        self.DRIVER.get(url)

    def go_to_login_page(self):
        """Goes to the login page.
        """
        self.logger.info('Going to login page.')
        try:
            self.WAIT.until(
                lambda d: d.find_element_by_id('nav-signin-tooltip')
            ).click()
            return True
        except TimeoutException:
            pass
        # If did not find "Sign in" button, click on "Accounts & Lists".
        nav_accountlist = self.WAIT.until(
            lambda d: d.find_element_by_id('nav-link-accountList')
        )
        
        try:
            self.ACTION.move_to_element(nav_accountlist).perform()
            self.WAIT.until(
                lambda d: d.find_element_by_id('nav-flyout-ya-signin')
            ).click()
            return True
        except TimeoutException:
            pass
        
        self.WAIT.until(
            ec.visibility_of_element_located((By.LINK_TEXT, 'Your Lists'))
        ).click()

    def fill_login_form(self, email, password):
        """Fills the login form with customer information.
        """
        self.logger.info('EMAIL: %s / PASSWORD: %s' % (email, password))
        self.WAIT.until(lambda d: d.find_element_by_id('ap_email')).send_keys(email)
        self.WAIT.until(lambda d: d.find_element_by_id('ap_password')).send_keys(password)
        self.WAIT.until(lambda d: d.find_element_by_id('signInSubmit')).click()

    def verification_needed(self):
        """Verifies if Amazon shows a verification page. And clicks on "Continue"
        or "Send Code" button.

        Return
        ------
        success : bool
            If True, Amazon took the robot to verification page. Otherwise,
            go shopping.
        """
        # Verification needed?
        try:
            verify_form_xpath = '//form[@name="claimspicker" and @action="verify"]'
            self.WAIT.until(lambda d: d.find_element_by_xpath(verify_form_xpath))
            self.WAIT.until(lambda d: d.find_element_by_id('continue')).click()
            self.logger.warning('Verification needed.')
            return True
        except TimeoutException:
            return False
    
    def wait_for_verification_code(self, email, password, local_timezone=None, attempts=10, time_bw_attempts=5):
        """Waits for the verification code arrives in customer mailbox.

        Parameters
        ----------
        email : str
            The email address.
        password : str
            The password of email address.
        local_timezone : str
            The timezone of local.
        attempts : int
            The number of times that the robot will attempt.
        time_bw_attempts : int
            The time between each attempt.
        
        Return
        ------
        code : str
            The verification code itself
        """
        if local_timezone:
            self.TIMEZONE = pytz.timezone(local_timezone)
        else:
            self.TIMEZONE = tzlocal.get_localzone()

        # Open an IMAP connection to wait for verification email
        self.logger.info('Waiting for verification code.')
        mail = MailBox(username=email,
                       password=password,
                       server='imap.mail.yahoo.com')
        mail.login()
    
        while (range(attempts)):
            # Current datetime
            now_dt = pytz.utc.localize(dt.utcnow())

            sleep(time_bw_attempts) # zZzZz...

            # Get emails from mailbox. 
            verification_emails = mail.get_emails_by_sender('account-update@amazon.com')
            verification_emails.reverse()

            for e in verification_emails:
                # Before compare datetime, convert to local timezone
                now_dt = now_dt.astimezone(self.TIMEZONE)            
                email_dt = e['date'].astimezone(self.TIMEZONE)

                # Email datetime is less than 10 minutes
                if not now_dt - email_dt > td(minutes=10):
                    date_format = '%H:%M:%S on %Y/%m/%d'
                    self.logger.info('A valid code was received in %s.' % email_dt.strftime(date_format))
                    
                    # Convert content to HTML and parse using xpath to search the code.
                    content_in_html = lxml.html.fromstring(e['content'])
                    amz_code = content_in_html.xpath('//p[@class="otp"]/text()')[0]
                    mail.logout()
                    return amz_code

        self.logger.warning('A verification email was not found.')                    
        mail.logout()

    def enter_the_code(self, code):
        """Enter the verification code.
        
        Parameters
        ----------
        code : str
            The code sent to customer email.
        """
        self.logger.info('Entering the code %s.' % code)
        self.DRIVER.find_element_by_xpath('//input[@name="code"]').send_keys(code)
        self.DRIVER.find_element_by_xpath('//input[@type="submit"]').click()

    def empty_cart(self):
        """Emptys the cart.
        """
        PLURAL = inflect.engine()
        try:
            cart_count = self.WAIT.until(lambda d: d.find_element_by_id('nav-cart-count')).text
            
            if cart_count != '0':
                self.logger.info('%s %s found on cart.' % (cart_count,
                                                           PLURAL.plural('item',cart_count)))
                self.WAIT.until(lambda d: d.find_element_by_id('nav-cart')).click()
                self.WAIT.until(lambda d: d.find_element_by_id('sc-active-cart'))
                while cart_count != '0':
                    products = self.WAIT.until(
                        lambda d: d.find_elements_by_xpath('//*[@data-asin and not(@data-removed)]'
                    ))
                    product = products.pop()
                    asin = product.get_attribute('data-asin')
                    product.find_element_by_xpath('.//input[@value="Delete"]').click()
                    WebDriverWait(self.DRIVER, self.WAIT_TIME).until(
                        ec.presence_of_element_located((By.XPATH,
                                                        '//*[@data-asin="%s" and @data-removed]' % asin))
                    )
                    cart_count = self.WAIT.until(lambda d: d.find_element_by_id('nav-cart-count')).text
                    self.logger.info('%s removed from cart.' % asin)
            else:
                self.logger.info('No item found on cart.')
        except TimeoutException:
            pass
    
    def type_on_search_bar(self, keyword):
        """Types a keyword on search bar.
        
        Parameters
        ----------
        keyword : str
            Keyword to be typed in searchbar
        """
        self.logger.info('KEYWORD: %s' % keyword)
        # Locates the search bar.
        self.DRIVER.execute_script('document.getElementById("twotabsearchtextbox").setAttribute("value", "%s")' % keyword)
        # Clicks in "Go"
        self.WAIT.until(lambda d: d.find_element_by_xpath(
            '//*[@id="nav-search-submit-text"]/following-sibling::input'
        )).click()

    def click_on_next(self):
        """Clicks on next button
        """
        try:
            WebDriverWait(self.DRIVER, 3).until(
                lambda d: d.find_element_by_xpath('//li[@class="a-last"]/a')
            ).click()
            return True
        except TimeoutException:
            pass

    def products_on_listing(self, asins, page_limit, permitted_conditions, stop_at_the_first=True):
        """Retrieves products on listing by ASIN codes.

        Parameters
        ----------
        asins : list
            The list with ASIN codes.
        page_limit : int
            The maximum number of pages that robot can access.
        stop_at_the_first : bool
            If True, the robot returns only the first found product.
        
        Returns
        -------
        products : list
            The list of found products (dicts).
        """
        self.logger.info('ASIN: %s' % asins)
        products = []
        
        for npage in range(1, page_limit + 1):
            # Gets the listing element on the page.
            listing = self.WAIT.until(
                lambda d: d.find_element_by_xpath('//*[@data-component-type="s-search-results"]')
            )
            # Searches for elements that contain informed ASIN codes.
            elmts_on_page = search_elmt_by_asin(listing=listing, asins=asins)
            # Analyzes each element.
            for elmt in elmts_on_page:
                product = {'date': dt.now().strftime('%Y-%m-%d %H:%M:%S'),
                           'asin': elmt.get_attribute('data-asin'),
                           'link': elmt.find_element_by_xpath(
                               './/a[contains(@class, "a-text-normal")]'
                            ).get_attribute('href'),
                           'page': npage,
                           'condition': get_eltm_status(elmt)}
                
                # Filter product by permitted condition
                if not product['condition'] in permitted_conditions:
                    self.logger.info('Product %s (%s) found on page %s. Disposed!' % (product['asin'],
                                                                                       product['condition'],
                                                                                       npage))
                    continue
                else:    
                    products.append(product)
                    self.logger.info('Product %s (%s) found on page %s.' % (product['asin'],
                                                                            product['condition'],
                                                                            npage))
                    # Returns only the first product.
                    if stop_at_the_first:
                        return products

            if page_limit == npage:
                self.logger.info('Page limit (%s) reached.' % page_limit)
                break
            
            # Go to next page.
            if not self.click_on_next():
                self.logger.info('Last page of the listing (%s) reached.' % npage)
                break

        if not products:
            self.logger.warning('No product found on listing.')

        return products