import pytz
import tzlocal
import logging
import lxml.html
from time import sleep
from random import randint
from selenium import webdriver
from customers.mailbox import MailBox
from selenium.webdriver.common.by import By
from datetime import datetime as dt, timedelta as td
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException, NoSuchElementException


def random_sleep(min=2, max=4):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.
    
    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
    """
    if max <= min:
        max = min + 3
    sleep(randint(min, max))

def get_logger(name):
    """Creates a instance logger

    Return
    ------
    logger : logging.Logger
        A logger itself.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # Set a handler
    if not logger.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        # Set a formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(process)s - %(message)s')
        handler.setFormatter(formatter)
        # Add handler to logger
        logger.addHandler(handler)
    return logger

class AccountCreatorRobot():
    def __init__(self, initial_page='https://amazon.com', wait_time=10, local_timezone=None):
        self.logger = get_logger('AccountCreatorRobot')
        self.logger.info('----> Initializing a robot.')
        self.INITITAL_PAGE = initial_page
        self.WAIT_TIME = wait_time
        # Local timezone
        if local_timezone:
            self.TIMEZONE = pytz.timezone(local_timezone)
        else:
            self.TIMEZONE = tzlocal.get_localzone()

    def _submit_proxy_credentials(self, proxy_user, proxy_pwd):
        """If use_proxy is True, fill the extension proxy form to authenticate on server.
        Avoids proxy authentication popup.

        Parameters
        ----------
        proxy_user: str
            The proxy username.
        proxy_pwd: str
            The proxy password.
        """
        self.WAIT.until(lambda d: d.find_element_by_id('login')).send_keys(proxy_user)
        self.WAIT.until(lambda d: d.find_element_by_id('password')).send_keys(proxy_pwd)
        self.WAIT.until(lambda d: d.find_element_by_id('save')).click()

    def create_browser(self,
                       chromedriver_path='chromedriver/chromedriver.exe',
                       use_proxy=True,
                       proxy_extension_path='chromedriver/ProxyAutoAuth2-0.crx',
                       proxy_host=None,
                       proxy_user='ZachKurian',
                       proxy_pwd='8e4bJZuP',
                       user_agent=None,
                       page_load_timeout=20,
                       load_images=False):
        """Creates a browser.

        Parameters
        ----------
        chromedriver_path : str
            The path to the driver executable.
        use_proxy : bool
            If True, the browser will use proxy.
        proxy_extension_path : str
            The path to proxy extension.
        proxy_host : str
            The proxy host (server:port). If not informed, the bot creates one.
        proxy_user: str
            The proxy username.
        proxy_pwd: str
            The proxy password.
        user_agent : str
            The USER_AGENT string. If use_login is True during ShoppingRobot
            class initialization,  the customer_ua parameter overrides this
            parameter.
        page_load_timeout : int
            The page load time out in seconds.
        load_images : bool
            If True, the browser will download images. Be carefull, it can
            overload the proxy
        """
        self.logger.info('A browser is being created.')
        opt = webdriver.ChromeOptions()
        opt.add_argument('--lang=en-us')
        opt.add_argument("--log-level=3")
        opt.add_argument("--silent")

        if not load_images:
            self.logger.info('Download images DISABLED.')
            prefs = {'profile.managed_default_content_settings.images':2}
            opt.add_experimental_option("prefs", prefs)
        else:
            self.logger.info('Download images ENABLED.')

        if use_proxy:
            if not proxy_host:
                proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
            opt.add_argument('--proxy-server=%s' % proxy_host)
            opt.add_extension(proxy_extension_path)
            self.logger.info('Proxy ENABLED: %s - %s / %s' % (proxy_host,
                                                              proxy_user,
                                                              proxy_pwd))
        else:
            self.logger.info('Proxy DISABLED.')

        self.logger.info('Page load time: %s seconds.' % page_load_timeout)

        if user_agent:
            opt.add_argument('user-agent=%s' % user_agent)

        self.DRIVER = webdriver.Chrome(executable_path=chromedriver_path,
                                       options=opt)
        self.DRIVER.set_page_load_timeout(page_load_timeout)
        self.ACTION = ActionChains(self.DRIVER)
        self.WAIT = WebDriverWait(self.DRIVER, self.WAIT_TIME)

        if use_proxy:
            self._submit_proxy_credentials(proxy_user, proxy_pwd)

    def go_to_initial_page(self):
        """Goes to initial page sets during initialization.
        """
        try:
            self.DRIVER.get(self.INITITAL_PAGE)
        except Exception:
            pass
        
        # No internet page?
        try:
            error = self.DRIVER.find_element_by_id('error-information-popup-content')
            error_msg = error.find_element_by_xpath('//*[@class="error-code"]').text
            self.logger.warning('No internet: %s' % error_msg)
            return False
        except NoSuchElementException:
            pass
        
        # Did not find the link HTML tag. So it did not work.
        try:            
            url = self.WAIT.until(
                lambda d: d.find_element_by_xpath('//link[@rel="canonical"]')
            ).get_attribute('href')
            self.logger.info('Accessing: %s' % url)
        except TimeoutException:
            self.logger.info('TimeoutException: Initial page did not load.')
            return False
        
        return True

    def quit(self):
        """Close the entire browser
        """
        self.logger.info('Closing the browser with id (%s).' % id(self.DRIVER) )
        self.DRIVER.quit()
    
    def go_to_account_creation_page(self):
        """Goes to "Create Account" form.
        """
        self.logger.info('Going to account creation form')
        # Click on "Start Here"
        WebDriverWait(self.DRIVER, self.WAIT_TIME).until(
            ec.visibility_of_element_located((By.LINK_TEXT, 'Start here.'))
        ).click()

        random_sleep() # zZzZzZz...
    
    def fill_form(self, name, email, password):
        """Fills the account creation form with provided information.

        Parameters
        ----------
        name : str
            The full name of customer.
        email : str
            The email address of customer.
        password : str
            The password of customer.
        """
        self.logger.info('Filling the form')
        # Fill "Your name" field
        self.WAIT.until(lambda d: d.find_element_by_id('ap_customer_name')).send_keys(name)

        random_sleep() # zZzZzZz...

        # Fill "Email" field
        self.WAIT.until(lambda d: d.find_element_by_id('ap_email')).send_keys(email)

        random_sleep() # zZzZzZz...

        # Fill "Password" field
        self.WAIT.until(lambda d: d.find_element_by_id('ap_password')).send_keys(password)

        random_sleep() # zZzZzZz...

        # Fill "Re-enter password" field
        self.WAIT.until(lambda d: d.find_element_by_id('ap_password_check')).send_keys(password)

        random_sleep() # zZzZzZz...
        
        # Click on Create an Amazon account
        self.WAIT.until(lambda d: d.find_element_by_id('continue')).click()

    def any_error_message(self):
        """Checks if page returns a error message.

        Return
        ------
        any_message : bool
            If True, the page presented an error message.
        """
        try:
            error_message = WebDriverWait(self.DRIVER, 2).until(
                ec.visibility_of_element_located((By.ID, 'auth-error-message-box'))
            ).find_element_by_xpath('//*[@class="a-alert-content"]')
            self.logger.warning(error_message.text)
            
            random_sleep() # zZzZzZz...

            return True
        except TimeoutException:
            return False

    def any_authportal_message(self):
        """Checks if page returns a authentication message.

        Return
        ------
        any_message : bool
            If True, the page presented an authentication message.
        """
        try:
            alerts = WebDriverWait(self.DRIVER, 2).until(
                ec.visibility_of_element_located((By.ID, 'authportal-main-section'))
            ).find_elements_by_xpath('//*[@class="a-box-inner a-alert-container"]')

            for alert in alerts:
                alert_msg = alert.text.replace('\n', ' ').replace('\r', '')
                if alert_msg:
                    self.logger.warning(alert_msg)
            
            random_sleep() # zZzZzZz...

            return True
        except TimeoutException:
            return False

    def verification_needed(self):
        """Verifies if Amazon shows a verification page. And clicks on "Continue",
        "Send Code" or "Verify "button.

        Return
        ------
        success : bool
            If True, Amazon took the robot to verification page. 
        """
        # Verification needed?
        try:
            self.WAIT.until(lambda d: d.find_element_by_xpath(
                '//input[@name="code" and @maxlength="6"]')
            )
            self.logger.warning('Verification needed.')
            return True
        except TimeoutException:
            return False
    
    def wait_for_verification_code(self, email, password, attempts=10, time_bw_attempts=5):
        """Waits for the verification code arrives in customer mailbox.

        Parameters
        ----------
        attempts : int
            The number of times that the robot will attempt.
        time_bw_attempts : int
            The time between each attempt.
        
        Return
        ------
        code : str
            The verification code itself
        """
        # Open an IMAP connection to wait for verification email
        self.logger.info('Waiting for verification code in %s attempts.' % attempts)
        mail = MailBox(username=email,
                       password=password,
                       server='imap.mail.yahoo.com')
        mail.login()
    
        while (range(attempts)):
            # Current datetime
            now_dt = pytz.utc.localize(dt.utcnow())

            sleep(time_bw_attempts) # zZzZz...

            # Get emails from mailbox. 
            verification_emails = mail.get_emails_by_subject('Verify your new Amazon account')
            verification_emails.reverse()

            for e in verification_emails:
                # Before compare datetime, convert to local timezone
                now_dt = now_dt.astimezone(self.TIMEZONE)            
                email_dt = e['date'].astimezone(self.TIMEZONE)

                # Email datetime is less than 10 minutes
                if not now_dt - email_dt > td(minutes=10):
                    date_format = '%H:%M:%S on %Y/%m/%d'
                    self.logger.info('An email with valid code was received in %s.' % email_dt.strftime(date_format))
                    
                    # Convert content to HTML and parse using xpath to search the code.
                    content_in_html = lxml.html.fromstring(e['content'])
                    amz_code = content_in_html.xpath('//p[@class="otp"]/text()')[0]
                    mail.logout()
                    return amz_code

        self.logger.warning('A verification email was not found.' % email)                    
        mail.logout()

    def enter_the_code(self, code):
        """Enter the verification code.
        
        Parameters
        ----------
        code : str
            The code sent to customer email.
        """
        self.logger.info('Entering the code %s on verification form.' % code)
        self.DRIVER.find_element_by_xpath('//input[@name="code"]').send_keys(code)
        random_sleep(min=1, max=2) # zZzZz...
        self.DRIVER.find_element_by_xpath('//input[@type="submit"]').click()

    def is_logged_in(self):
        """Checks if customer is logged in.

        Return
        ------
        is_logged_in : bool
            If True, customer is logged in. 
        """
        try:
            self.WAIT.until(lambda d: d.find_element_by_xpath(
                '//*[@id="nav-link-accountList"]/span[text()="Hello, Sign in"]'
            ))
            self.logger.info('Customer did not log in.')
            return False
        except TimeoutException:
            self.logger.info('Customer logged in.')
            return True
