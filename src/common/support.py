import os
import csv
import logging
import configparser
from random import choice, random, randint


def split_and_strip(string, delimiter):
    """Split and strip a string by a delimiter
    
    Parameters
    ----------
    string : str
        The string to handle.
    delimiter : str
        The delimiter use to slit the string

    Returns
    -------
    list : list
        A list with split and stripped string
    """
    return [k.strip() for k in string.split(delimiter)]

def get_a_product(file, delimiter=';', quote='"'):
    """From a CSV file, the function randomly chooses one product to use.
    
    Parameters
    ----------
    file : str
        The file that contains all products information.
    delimiter : str
        The delimiter used to read the CSV file
    quote : str
        The quote character use to read the CSV file

    Returns
    -------
    product : dict
        A dict that contains information about the chosen product
    """
    # 'file' arg must be a file and exist
    assert os.path.isfile(file) is True, 'File %s does not exist or not a file.' % file
    with open(file, 'r') as f:
        reader = csv.reader(f, delimiter=delimiter, quotechar=quote)
        # Skip header
        next(reader)
        # Start reading
        products = []
        for row in reader:
            asins = split_and_strip(row[0], ',')
            keywords = split_and_strip(row[1], ',')
            weights_str = split_and_strip(row[2], ',')
            # Convert weights to float
            weights = [float(w) for w in weights_str]
            p = {'asins': asins,
                 'keywords': keywords,
                 'weights': weights}
            products.append(p)

    product = choice(products)
    return product

def get_a_customer(file, delimiter=';', quote='"'):
    """Choose randomically a customer from a CSV file
    
    Parameters
    ----------
    file : str
        A file path with user agents information. This file must be a CSV file
        type, separated with semicolon and using double quotes '"'.
    
    Returns
    -------
    chosen_customer : dict
        A dictionary with information about the chosen customer.
    """
    customers = []
    with open(file, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=delimiter, quotechar=quote)
        # Skip the headers
        next(reader, None)
        for r in reader:
            customer = dict(r)
            customers.append(customer)
    # Choose only customer with Amazon accounts.
    while True:            
        chosen_customer = choice(customers)
        if chosen_customer['Amazon account created'] == 'OK':
            break

    return chosen_customer

def weighted_choice(sequence, weights):
    """Receives a sequence and its weights to choose one element of the sequence.
    - Sequence and weights must be the same lenght
    - Sum of weights need be 1
    
    Parameters
    ----------
    sequence : list
        A sequence
    weights : list

    Returns
    -------
    element : str
        A element of the sequence
    """
    weights = [float(w) for w in weights]
    assert len(weights) == len(sequence), 'Keywords and weights must be same lenght'
    assert abs(1. - sum(weights)) < 1e-6, 'Sum of weights need be 1'
    x = random()
    for i, elmt in enumerate(sequence):
        if x <= weights[i]:
            return elmt
        x -= weights[i]

def read_config(file):
    """Reads .ini file from absolute path
    
    Parameters
    ----------
    file : str
        a .ini file

    Returns
    -------
    config : configparser.ConfigParser
        A ConfigParser object
    """
    file_path = os.path.abspath(file)
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(file_path)
    return config

def load_config(file):
    """Loads the config file into a dict.
    
    Parameters
    ----------
    file : str
        The .ini file with configurations.
    
    Return
    ------
    cfg : dict
        The dict with the configurations.
    """
    config = read_config(file)
    
    # Proxy host
    server = config.get('PROXY', 'server')
    port = config.get('PROXY', 'port')
    if not (server or port):
        proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
    else:    
        proxy_host = '%s:%s' % (server, port)

    cfg = {'initial_page': config.get('DRIVER', 'initial_page'),
           'wait_time': config.getint('DRIVER', 'wait_time'),
           'chromedriver_path': config.get('DRIVER', 'exe'),
           'page_load_timeout': config.getint('DRIVER', 'page_load_timeout'),
           'page_limit': config.getint('DRIVER', 'page_limit'),
           'load_images': config.getboolean('DRIVER', 'load_images'),
           'use_login': config.getboolean('LOGIN', 'active'),
           'customers': config.get('LOGIN', 'customers'),
           'local_timezone': config.get('LOGIN', 'local_timezone'),
           'use_proxy': config.getboolean('PROXY', 'active'),
           'proxy_extention': config.get('PROXY', 'extention'),
           'proxy_host': proxy_host,
           'proxy_user': config.get('PROXY', 'user'),
           'proxy_pwd': config.get('PROXY', 'pwd'),
           'products': config.get('PRODUCTS', 'file')}
    
    return cfg

def load_customers(file):
    """Loads all customers from a CSV file.
    file : str
        The CSV file with customers information.

    Returns
    -------
    customers : list
        A list of dictionaries with customers information loaded from file.
    """
    customers = []
    with open(file) as f:
        reader = csv.DictReader(f)
        for r in reader:
            customers.append(dict(r))
    f.close()
    
    return customers

def save_customers(file, customers):
    """Saves customers list of dictionaries on CSV file.
    file : str
        The CSV file with customers information.
    customers : list
        A list of dictionaries with customers information.
    """
    header = list(customers[0].keys())

    with open(file, 'w', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=header)
        writer.writeheader()
        writer.writerows(customers)
    f.close()

def filter_list(_list, key, values):
    filtered_list = []
    for k, v in values.items():
        if v:
            temp_list = [l for l in _list if l[key] == k]
            filtered_list = filtered_list + temp_list

    return filtered_list

def get_logger(name):
    """Creates a instance logger

    Return
    ------
    logger : logging.Logger
        A logger itself.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # Set a handler
    if not logger.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        # Set a formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(process)s - %(message)s')
        handler.setFormatter(formatter)
        # Add handler to logger
        logger.addHandler(handler)
    return logger

def get_permitted_conditions(condition_option):
    """Get the permitted conditions based the option passed by command line.
    
    Parameters
    ----------
    condition_option : str
        One of three options: 'sponsored', 'non sponsored' or 'both'.
    Returns
    -------
    permitted_conditions: list
        The permitted conditions.
    """
    if condition_option == 'both':
        permitted_conditions = ['sponsored', 'non sponsored']
    else:
        permitted_conditions = [condition_option]
    
    return permitted_conditions