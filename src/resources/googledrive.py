import json
import gspread
from resources.utility import get_logger
from googleapiclient import discovery
from googleapiclient.errors import HttpError
from oauth2client.service_account import ServiceAccountCredentials


class GoogleDrive():
    def __init__(self, credentials_file):
        self.logger = get_logger('GoogleDrive')
        scope = ['https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_file,
                                                                       scope)
        self.logger.info('Credentials: (%s).' % credentials_file)
        
        # Google Drive service object.
        self.SERVICE = discovery.build('drive',
                                       'v3',
                                       credentials=credentials,
                                       cache_discovery=False)
    
    def create_folder(self, name, parent=None):
        search_query = 'name = "%s"' \
                       ' and mimeType = "application/vnd.google-apps.folder"' % name

        if parent:
            search_query = ''.join([search_query,
                                    ' and "%s" in parents' % parent['id']])

        # Avoid folders with the same name.
        exists_folder = self.SERVICE.files().list(q=search_query).execute().get('files')
       
        if exists_folder:
            self.logger.warning('The "%s" folder already exists.' % name)
            return exists_folder[0]
        else:
            # Otherwise, continues with folder creation.
            folder_body = {'name': name,
                           'mimeType': 'application/vnd.google-apps.folder'}
            if parent:
                folder_body['parents'] = [parent['id']]

            folder = self.SERVICE.files().create(body=folder_body).execute()
            self.logger.info('Folder "%s" created.' % folder['name'])

            return folder

    def create_spreadsheet(self, name, parent=None):
        search_query = 'name = "%s"' \
                       ' and mimeType = "application/vnd.google-apps.spreadsheet"' % name

        if parent:
            search_query = ''.join([search_query,
                                    ' and "%s" in parents' % parent['id']])

        # Avoid folders with the same name.
        exists_spreadsheet = self.SERVICE.files().list(q=search_query).execute().get('files')
       
        if exists_spreadsheet:
            self.logger.warning('The "%s" spreadsheet already exists.' % name)
            return exists_spreadsheet[0]
        else:
            # Otherwise, continues with folder creation.
            spreadsheet_body = {'name': name,
                                'mimeType': 'application/vnd.google-apps.spreadsheet'}
            if parent:
                spreadsheet_body['parents'] = [parent['id']]

            spreadsheet = self.SERVICE.files().create(body=spreadsheet_body).execute()
            self.logger.info('Spreadsheet "%s" created.' % name)

            return spreadsheet
        
    def share(self, metadata, perm_type, perm_role, perm_email):
        batch = self.SERVICE.new_batch_http_request()
        user_permission = {'type': perm_type,
                           'role': perm_role,
                           'emailAddress': perm_email}
        batch.add(self.SERVICE.permissions().create(fileId=metadata['id'],
                                                    body=user_permission,
                                                    fields='id',))
        batch.execute()
        self.logger.info('%s has %s permission in "%s".' % (perm_email, perm_role, metadata['name']))

    def delete(self, metadata):
        try:
            self.SERVICE.files().delete(fileId=metadata['id']).execute()
            self.logger.info('Resource "%s" deleted.' % metadata['name'])
        except HttpError as error:
            error_msg = json.loads(error.content).get('error').get('message')
            self.logger.error(error_msg)
    
    def delete_all_folders(self):
        search_query = 'mimeType = "application/vnd.google-apps.folder"'
        folders = self.SERVICE.files().list(q=search_query).execute()
        for folder in folders.get('files'):
            self.delete(folder)

    def delete_all_files_from_folder(self, folder):
        search_query = 'mimeType != "application/vnd.google-apps.folder" and "%s" in parents' % folder['id']
        files = self.SERVICE.files().list(q=search_query).execute()
        for file in files.get('files'):
            self.delete(file)

    def get_url(self, _id):
        return self.SERVICE.files().get(fileId=_id, fields='webViewLink').execute()

    def delete_everything(self):
        resources = self.SERVICE.files().list().execute()
        for resource in resources.get('files'):
            self.delete(resource)

    def create(self, metadata):
        search_query = 'name = "%s"' \
                       ' and mimeType = "%s"' % (metadata['name'],
                                                 metadata['mimeType'])
        if metadata.get('parents'):
            search_query = ''.join([search_query,
                                    ' and "%s" in parents' % ''.join( metadata['parents'])])

        # Avoid folders with the same name.|
        exists_file = self.SERVICE.files().list(q=search_query).execute().get('files')

        if exists_file:
            self.logger.info('The "%s" file already exists.' % metadata['name'])
            return exists_file[0]
        else:
            file = self.SERVICE.files().create(body=metadata).execute()
            self.logger.info('File "%s" created: ID (%s).' % (file['name'], file['id']))

            return file
        