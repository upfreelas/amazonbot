import re
import logging
import argparse
from operations import search_operation, open_operation


def check_asin(s, m=re.compile(r'^[a-zA-Z0-9]{10}$')):
    """Checks if a string matches with an ASIN code pattern.

    Parameters
    ----------
    s : string
        The string to be checked.
    
    Returns
    -------
    s : string
        The string itself. If the pattern was not found on the string, raises an ArgumentTypeError.
    """
    if not m.match(s):
        raise argparse.ArgumentTypeError('"%s" is not like an ASIN code.' % s)
    return s

def parse_arguments():
    """Parses the arguments passed by command line.
    
    Returns
    -------
    args : argparse.ArgumentParser
        The object with all parsed arguments
    """
    # Common arguments
    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument('-u',
                               dest='username',
                               action='store',
                               help='A username (email) to sign in before it makes an operation.')
    common_parser.add_argument('-p',
                               dest='password',
                               action='store',
                               help='A password to sign in before it makes an operation..')
    common_parser.add_argument('-e',
                               dest='empty_cart',
                               action='store_true',
                               help='Empties the cart before it makes an operation.')

    # Main parser
    parser = argparse.ArgumentParser(description='Script to operate SimplegRobot.',
                                     prog='python src\\operate.py',
                                     usage='%(prog)s <operation> [options]')

    subparsers = parser.add_subparsers(title='operations',
                                       help='Select a operation.')
    subparsers.required = True
    subparsers.dest = 'operation'

    # BY KEYWORD
    search_parser_msg = 'Search the product page by keyword.'
    search_parser = subparsers.add_parser('search',
                                          description=search_parser_msg,
                                          parents=[common_parser],
                                          prog='python src\\operate.py search',
                                          usage='%(prog)s <asin(s)> <keyword> [options]',
                                          help=search_parser_msg)
    search_parser.set_defaults(func=search_operation)
    search_parser.add_argument('-a',
                               nargs='+',
                               required=True,
                               dest='asins',
                               type=check_asin,
                               help='One or more ASIN codes. REQUIRED!')
    search_parser.add_argument('-k',
                               required=True,
                               dest='keyword',
                               action='store',
                               help='The keyword. REQUIRED!')
    search_parser.add_argument('-c',
                               dest='condition_option',
                               action='store',
                               choices=['sponsored', 'non sponsored', 'both'],
                               default='both',
                               help='Define a condition like sponsored, non sponsored or both. Default condition is both.')
    search_parser.add_argument('-f',
                               dest='stop_at_the_first',
                               action='store_true',
                               help='Stops the search at the first occurrence.')
    search_parser.add_argument('-l',
                               dest='page_limit',
                               type=int,
                               default=21,
                               help='Stops the search when the page limit is reached. Default is 21')

    # BY URL
    url_parser_msg = 'Open the product page directly.'
    url_parser = subparsers.add_parser('open',
                                       description=url_parser_msg,
                                       parents=[common_parser],
                                       prog='python src\\operate.py open',
                                       usage='%(prog)s <asin> <URL> [options]',
                                       help=url_parser_msg)
    url_parser.set_defaults(func=open_operation)
    url_parser.add_argument('-a',
                            required=True,
                            dest='asin',
                            type=check_asin,
                            help='The ASIN code. REQUIRED!')
    url_parser.add_argument('-U',
                            dest='url',
                            action='store',
                            help='The URL to access.')

    # Instantiate the parser.
    args = parser.parse_args()
    
    # Argument verifications
    if bool(args.username) ^ bool(args.password):
        parser.error('username (-u) and password (-p) must be given together')
    
    if bool(args.empty_cart) and not(bool(args.username) and bool(args.password)):
        parser.error('empty_cart (-e) must be used with username (-u) and password (-p)')
    
    return args

def main():
    options = parse_arguments()
    
    print('\noperate.py script parameters:')
    for opt in vars(options):
        print(' - %s: %s' % (opt, getattr(options, opt)))

    options.func(options)

if __name__ == "__main__":
    main()