from random import choice
from common import support
from robots.shoppingrobot import ShoppingRobot
from resources.googlespreadsheet import GoogleSpreadSheet


def run_a_thread():
    cfg = support.load_config('shoppingrobot.ini')
    gspreadsheet = GoogleSpreadSheet(credentials_file='credentials/amazonbot-google_sheets.json')
    customers = gspreadsheet.get_selected_rows(title='customers',
                                            columns_as_list=['specific_asins'],
                                            columns_as_bool=['select', 'empty_cart', 'add_to_wish_list', 'sponsored',
                                                                'non_sponsored', 'purchase_the_first'])
    customer = choice(customers)

    if customer['specific_asins']:
        all_products = gspreadsheet.get_all_spreadsheet('products',
                                                        columns_as_list=['asins', 'keywords', 'weights'],
                                                        columns_as_bool=['select'])
        products = []
        for asin in customer['specific_asins']:
            for product in all_products:
                if asin in product['asins']:
                    product['asins'] = [asin]
                    products.append(product)
    else:
        products = gspreadsheet.get_selected_rows(title='products',
                                                columns_as_list=['asins', 'keywords', 'weights','asin'],
                                                columns_as_bool=['select'])

    product = choice(products)
    keyword = support.weighted_choice(sequence=product['keywords'], weights=product['weights'])
    purchase_the_first = customer['purchase_the_first']
    try:
        while True:
            robot = ShoppingRobot(use_login=cfg['use_login'],
                                wait_time=cfg['wait_time'],
                                customer_email=customer['Email'],
                                customer_pwd=customer['Password'],
                                local_timezone=cfg['local_timezone'])
            robot.create_browser(chromedriver_path='chromedriver/chromedriver.exe',
                                proxy_extension_path='chromedriver/ProxyAutoAuth2-0.crx',
                                use_proxy=cfg['use_login'],
                                user_agent=customer['user_agent'])

            if not robot.go_to_initial_page():
                robot.quit()
                continue
            
            # Login process

            robot.sign_in()
            robot.fill_login_form()
            
            if robot.verification_needed():
                code = robot.wait_for_verification_code()
                robot.enter_the_code(code)
            
            # 
            #if customer['empty_cart']:
            #    robot.empty_cart()


            #products_on_listing = robot.products_on_listing(asins=product['asins'],
            #                                                keyword=keyword,
            #                                                page_limit=cfg['page_limit'],
            #                                                stop_at_the_first=purchase_the_first)

            #for p in products_on_listing:
            #    gspreadsheet.append_row_by_dict(title='products_tracking', _dict=p)
 
            #values = {'sponsored': customer['sponsored'],
            #          'non_sponsored': customer['non_sponsored']}

            #products_on_listing = support.filter_list(products_on_listing, 'status_on_listing', values)
            
            #if not products_on_listing:
            #    break
         
            #if not purchase_the_first:
            #    break
            
            #product_link = products_on_listing[0]['link']
            #robot.get(product_link)
            
            #if customer['add_to_wish_list']:
            #   robot.add_to_wish_list()

            # Product page actions
            #robot.see_images()
            #robot.see_and_choose_a_color()
            #robot.scroll_down()
            #robot.scroll_up()
            #robot.see_popup_reviews()
            #currentHeight, finalHeight = robot.see_reviews()
            #robot.scroll_down(currentHeight, finalHeight)
            #robot.scroll_up()
            #robot.choose_quantity()
            print('Before identifying asin')
            print('Asin is : 1795687614')
            robot.type_on_search_bar(keyword=keyword)

            robot.go_to_atc_page(asin='1795687614')


            # Add to cart and checkout process!
            # if robot.add_to_cart():
            #     robot.add_to_your_order_modal()
            #     robot.add_to_your_order_side_pane()
            #
            # robot.click_on_proceed_to_checkout()
            # robot.find_shipping_address_form()
            row = robot.get_output()
            gspreadsheet.append_row(title='shoppingrobot', header=robot.HEADER, row=row)

            break
    except Exception:
        print('!!!!!!!!!!! An unexpected error occured. !!!!!!!!!!')
        robot.quit()

    robot.quit()

def main():
    i = 1
    while True:
        print('\n===== Interaction %s' % i)
        run_a_thread()
        i+=1

if __name__ == "__main__":
    main()
