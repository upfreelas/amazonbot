from common.support import read_config
from robots.simplerobot import SimpleRobot
from common.support import get_logger, get_permitted_conditions


def prepare_robot(email=None, password=None, empty_cart=None):
    """Creates and prepares a Robot to access page. Initial browser parameters are set here too.
    
    Parameters
    ----------
    email : str
        The email address to sign in.
    password : str
        The password to sign in.
    emptt_cart : bool
        If True, empties cart after sign in.

    Returns
    -------
    robot : SimpleRobot
        The robot to access page.
    """
    cfg = read_config('simplerobot.ini')
    CHROMEDRIVER_PATH = cfg.get('SETTINGS', 'chromedriver_path', fallback='chromedriver/chromedriver.exe')
    INITIAL_PAGE = cfg.get('SETTINGS', 'initial_page', fallback='https://amazon.com')
    LOAD_IMAGES = cfg.getboolean('SETTINGS', 'load_images', fallback=False)
    LOCAL_TIMEZONE = cfg.get('SETTINGS', 'local_timezone', fallback=None)
    PROXY_EXTENSION_PATH = cfg.get('SETTINGS', 'proxy_extension_path', fallback=None)
    PROXY_USER = cfg.get('SETTINGS', 'proxy_user', fallback=None)
    PROXY_PWD = cfg.get('SETTINGS', 'proxy_pwd', fallback=None)

    # Create the robot
    robot = SimpleRobot()
    robot.create_browser(chromedriver_path=CHROMEDRIVER_PATH,
                         proxy_extension_path=PROXY_EXTENSION_PATH,
                         proxy_user=PROXY_USER,
                         proxy_pwd=PROXY_PWD,
                         load_images=LOAD_IMAGES)
    robot.get(INITIAL_PAGE)

    if email and password:
        robot.go_to_login_page()
        robot.fill_login_form(email=email, password=password)
        
        if robot.verification_needed():
            code = robot.wait_for_verification_code(email=email,
                                                    password=password,
                                                    local_timezone=LOCAL_TIMEZONE)
            robot.enter_the_code(code)
        
        if empty_cart:
            robot.empty_cart()
    
    return robot

def search_operation(options):
    """Searches the product page by a keyword.
    
    Parameters
    ----------
    options : argparse.ArgumentParser
        The object with all parsed arguments passed by command-line.
    """
    logger = get_logger('SearchOperation')
    # Prepare Robot
    robot = prepare_robot(email=options.username,
                          password=options.password,
                          empty_cart=options.empty_cart)
    # Algorithm
    ASINS = options.asins
    KEYWORD = options.keyword
    PAGE_LIMIT = options.page_limit
    STOP_AT_THE_FIRST = options.stop_at_the_first
    PERMITTED_CONDITIONS = get_permitted_conditions(condition_option=options.condition_option)

    robot.type_on_search_bar(keyword=KEYWORD)
    products = robot.products_on_listing(asins=ASINS,
                                         page_limit=PAGE_LIMIT,
                                         permitted_conditions=PERMITTED_CONDITIONS,
                                         stop_at_the_first=STOP_AT_THE_FIRST)
        
    if not products:
        logger.info('No item found.')
    # Show the output.
    if STOP_AT_THE_FIRST:
        logger.info('Stoping at the first product.')
        product_link = products[0]['link']
        robot.get(product_link)
    else:
        logger.info('Products found:')
        for i, p in enumerate(products, 1):
            logger.info(' Product %s' % i)
            for k in p.keys():
                logger.info('   %s: %s' % (k, p[k]))
    
    print('\nNow is up to you. Good luck!')

def open_operation(options):
    """Goes to the product page by a URL.
    
    Parameters
    ----------
    options : argparse.ArgumentParser
        The object with all parsed arguments passed by command-line.
    """
    logger = get_logger('OpenOperation')
    # Prepare Robot
    robot = prepare_robot(email=options.username,
                          password=options.password,
                          empty_cart=options.empty_cart)
    
    # Algorithm
    ASIN = options.asin
    URL = options.url
    
    if URL:
        logger.info('URL was informed.')
        robot.get(URL)
    else:
        logger.info('URL was not informed. Assembling one.')
        robot.get('https://www.amazon.com/dp/%s' % ASIN)

    print('\nNow is up to you. Good luck!')
